package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root,800,720);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) throws Exception {
//		DatabaseUtil dbUtil = new DatabaseUtil();
//		dbUtil.setUp();
////		dbUtil.startTransaction();
//
////		dbUtil.createAnimal("Feli", "pisica", 10.0, 14, "mare");
////		dbUtil.createAnimal("Alma", "caine", 3.5, 5, "mica");
////		dbUtil.createAnimal("Aky", "caine", 6.7, 3, "medie");
////		dbUtil.createAnimal("Floppy","pisica", 3.7, 6, "mica");
////		dbUtil.createAnimal("Bob", "caine", 4.5, 2, "medie");
////		dbUtil.readAnimal(1);
////		dbUtil.updateAnimal(1, "Felix", "pisica", 10.0, 14, "mare");
////		dbUtil.updateAnimal(2, "Almma", "caine", 3.5, 5, "mica");
////		dbUtil.readAnimal(2);
////		dbUtil.deleteAnimal(4);
//		
//		
////		dbUtil.createPersonalMedical("Ardelean","Timotei", "0773215649", "L-V 10:00 - 18:00");
////		dbUtil.createPersonalMedical("Rether", "Monica", "0726489657", "L-V 08:00 - 12:00 14:00 - 18:00");
////		dbUtil.createPersonalMedical("Corbu", "Raluca","0735498621", "L-V 12:00 - 20:00");
////		dbUtil.createPersonalMedical("Anghel","Mariana", "0754987564", "L-V 9:00 - 17:00");
////		dbUtil.createPersonalMedical("Barbuta", "Adrian", "0744669585", "L-V 12:00 - 20:00");
////		dbUtil.readPersonalMedical(1);
////		dbUtil.updatePersonalMedical(3, "Corbu", "Raluca","0735498621", "L-V 12:00 - 20:00");
////		dbUtil.createPersonalMedical(6, "Avram", "Simona", "0759846836", "L-V 12:00 - 20:00");
////		dbUtil.deletePersonalMedical(6);
//		
//
//		
//		
////		dbUtil.createProgramare(1, dbUtil.readAnimal(1), dbUtil.readPersonalMedical(2), new Date(2018,5,10,16,0));
////		dbUtil.createProgramare(2, dbUtil.readAnimal(3), dbUtil.readPersonalMedical(1), new Date(118,10,30,18,30));
////		dbUtil.createProgramare(3, dbUtil.readAnimal(2), dbUtil.readPersonalMedical(2), new Date(118,11,25,12,30));
////		dbUtil.createProgramare(4, dbUtil.readAnimal(4), dbUtil.readPersonalMedical(5), new Date(118,3,20,13,30));
////		dbUtil.createProgramare(5, dbUtil.readAnimal(5), dbUtil.readPersonalMedical(4), new Date(118,6,18,10,30));
////		dbUtil.updateProgramare(1, dbUtil.readAnimal(1), dbUtil.readPersonalMedical(2), new Date(118,5,10,16,0));
////		dbUtil.updateProgramare(3, dbUtil.readAnimal(2), dbUtil.readPersonalMedical(3), new Date(118,11,25,12,30));
////		dbUtil.deleteProgramare(3);
//		
//		dbUtil.sortareDupaData();
//		
//		
//		
//		
////		afisez programarea cu ordinul = 0 a animalului cu id = 3 
////		System.out.println(dbUtil.readAnimal(3).getProgramares().get(0).getData());
//		
////		dbUtil.printAllAnimalsFromDB();
//		
//		dbUtil.closeEntityManager();
//		
		launch(args);
	}



}
