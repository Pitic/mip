package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	private String tip;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to PersonalMedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	private PersonalMedical personalmedical;

	public Programare() {
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTip() {
		return this.tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public PersonalMedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(PersonalMedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}