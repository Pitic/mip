package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.ResourceBundle;

import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser.selectItem_return;

import com.sun.org.apache.bcel.internal.generic.Select;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import util.DatabaseUtil;
import model.Animal;
import model.Programare;

public class MainController implements Initializable {

	@FXML
	private ListView<String> listView;

	public void populateMainListView() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		listView.setItems(animalNamesList);
		listView.refresh();
		db.closeEntityManager();
	}
	@FXML
	private ImageView img;
	@FXML
	private TextField name;
	@FXML
	private TextField age;
	@FXML
	private TextField weight;
	@FXML
	private TextField owner;
	@FXML
	private TextField phone;
	@FXML
	private TextField tip;
	@FXML
	private TextField mail;
	@FXML
	private TextField rasa;
	@FXML
	private Text nameText;
	@FXML
	private Text ageText;
	@FXML
	private Text weightText;
	@FXML
	private Text phoneText;
	@FXML
	private Text ownerText;
	@FXML
	private Text mailText;
	@FXML
	private Text breedText;
	@FXML
    public void handleMouseClick(MouseEvent event) {
		String s=listViewDate.getSelectionModel().getSelectedItem();
		LocalDate date = selectDate.getValue();
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Programare> programari = (List<Programare>) db.progamariList();
		for (Programare p : programari)
			if (new SimpleDateFormat("yyyy-MM-dd").format(p.getData()).equals(date.toString())) 
				if(p.getData().toString().substring(11, 16).equals(s.substring(0, 5)))
				{
					name.setText(p.getAnimal().getNume());
					age.setText(Integer.toString(p.getAnimal().getVarsta()));
					weight.setText(Double.toString(p.getAnimal().getGreutate()));
					owner.setText(p.getAnimal().getProprietar());
					phone.setText(p.getAnimal().getTelefon());
					mail.setText(p.getAnimal().getMail());
					tip.setText(p.getTip());
					rasa.setText(p.getAnimal().getRasa());
					if(p.getAnimal().getRasa().equals("caine"))
						try {
							img.setImage(new Image(new FileInputStream("C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\doggo.jpeg"),200,200,false,false));
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					else
						try {
							img.setImage(new Image(new FileInputStream("C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\cat.jpeg"),200,200,false,false));
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					name.setVisible(true);
					age.setVisible(true);
					weight.setVisible(true);
					owner.setVisible(true);
					phone.setVisible(true);
					mail.setVisible(true);
					tip.setVisible(true);
					rasa.setVisible(true);
					nameText.setVisible(true);
					ageText.setVisible(true);
					weightText.setVisible(true);
					ownerText.setVisible(true);
					phoneText.setVisible(true);
					mailText.setVisible(true);
					breedText.setVisible(true);
			}

    		
		 
    }

	@FXML
	private TextField display;
	@FXML
	private DatePicker selectDate;
	@FXML
	private ListView<String> listViewDate;

	@FXML
	private void getDateAction(ActionEvent event) {
		LocalDate date = selectDate.getValue();
		if (date != null) {
			DatabaseUtil db = new DatabaseUtil();
			db.setup();
			db.startTransaction();
			List<Programare> programari = (List<Programare>) db.progamariList();
			ObservableList<String> dates = FXCollections.observableArrayList();
			for (Programare p : programari)
				if (new SimpleDateFormat("yyyy-MM-dd").format(p.getData()).equals(date.toString())) {
					dates.add(p.getData().toString().substring(11, 16) + " " + p.getTip());
				}
			Collections.sort(dates);
			listViewDate.setItems(dates);
			listViewDate.refresh();
			db.closeEntityManager();
		} else {
			display.setText("");
		}

	}

	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals)
			names.add(a.getNume());
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		populateMainListView();
		name.setVisible(false);
		age.setVisible(false);
		weight.setVisible(false);
		owner.setVisible(false);
		phone.setVisible(false);
		mail.setVisible(false);
		tip.setVisible(false);
		rasa.setVisible(false);
		nameText.setVisible(false);
		ageText.setVisible(false);
		weightText.setVisible(false);
		ownerText.setVisible(false);
		phoneText.setVisible(false);
		mailText.setVisible(false);
		breedText.setVisible(false);
	}
}
