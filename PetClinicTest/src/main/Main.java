package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

public class Main extends Application {

	public static void singletone() throws IOException {
		DatabaseUtil db = new DatabaseUtil();
		try {
			db.setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		String animal = "";
		for (Animal a : animalDBList) {
			animal += a.getNume() + ";" + a.getProprietar() + ";" + a.getTelefon() + "\n";
		}
		BufferedWriter writer;
		writer = new BufferedWriter(
				new FileWriter("C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\PetClinicTest\\Animals.txt"));
		writer.write(animal);
		writer.close();
		String filename = "C:\\\\Users\\\\Raluca\\\\Desktop\\\\Facultate\\\\MIP\\\\PetClinicTest\\\\Animals.txt";
		Path path = Paths.get(filename);
		BufferedReader br = Files.newBufferedReader(path);
		String input;
		try {
			while ((input = br.readLine()) != null) {
				String[] items = input.split(";");
				String petName = items[0];
				String ownerName = items[1];
				String telephone = items[2];

				System.out.println(petName + "     - Pet Name");
				System.out.println(ownerName + "     - Owner Name");
				System.out.println(telephone + "     - Telephone");
				System.out.println();
			}
		} finally {
			if (br != null)
				br.close();
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
//			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
//			Scene scene = new Scene(root, 850, 620);
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/LogIn.fxml"));
			Scene scene = new Scene(root, 400, 400);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {
//		DatabaseUtil dbUtil = new DatabaseUtil();
//		dbUtil.setUp();
////		dbUtil.startTransaction();
//
////		dbUtil.createAnimal("Feli", "pisica", 10.0, 14, "mare");
////		dbUtil.createAnimal("Alma", "caine", 3.5, 5, "mica");
////		dbUtil.createAnimal("Aky", "caine", 6.7, 3, "medie");
////		dbUtil.createAnimal("Floppy","pisica", 3.7, 6, "mica");
////		dbUtil.createAnimal("Bob", "caine", 4.5, 2, "medie");
////		dbUtil.readAnimal(1);
////		dbUtil.updateAnimal(1, "Felix", "pisica", 10.0, 14, "mare");
////		dbUtil.updateAnimal(2, "Almma", "caine", 3.5, 5, "mica");
////		dbUtil.readAnimal(2);
////		dbUtil.deleteAnimal(4);
//		
//		
////		dbUtil.createPersonalMedical("Ardelean","Timotei", "0773215649", "L-V 10:00 - 18:00");
////		dbUtil.createPersonalMedical("Rether", "Monica", "0726489657", "L-V 08:00 - 12:00 14:00 - 18:00");
////		dbUtil.createPersonalMedical("Corbu", "Raluca","0735498621", "L-V 12:00 - 20:00");
////		dbUtil.createPersonalMedical("Anghel","Mariana", "0754987564", "L-V 9:00 - 17:00");
////		dbUtil.createPersonalMedical("Barbuta", "Adrian", "0744669585", "L-V 12:00 - 20:00");
////		dbUtil.readPersonalMedical(1);
////		dbUtil.updatePersonalMedical(3, "Corbu", "Raluca","0735498621", "L-V 12:00 - 20:00");
////		dbUtil.createPersonalMedical(6, "Avram", "Simona", "0759846836", "L-V 12:00 - 20:00");
////		dbUtil.deletePersonalMedical(6);
//		
//
//		
//		
////		dbUtil.createProgramare(1, dbUtil.readAnimal(1), dbUtil.readPersonalMedical(2), new Date(2018,5,10,16,0));
////		dbUtil.createProgramare(2, dbUtil.readAnimal(3), dbUtil.readPersonalMedical(1), new Date(118,10,30,18,30));
////		dbUtil.createProgramare(3, dbUtil.readAnimal(2), dbUtil.readPersonalMedical(2), new Date(118,11,25,12,30));
////		dbUtil.createProgramare(4, dbUtil.readAnimal(4), dbUtil.readPersonalMedical(5), new Date(118,3,20,13,30));
////		dbUtil.createProgramare(5, dbUtil.readAnimal(5), dbUtil.readPersonalMedical(4), new Date(118,6,18,10,30));
////		dbUtil.updateProgramare(1, dbUtil.readAnimal(1), dbUtil.readPersonalMedical(2), new Date(118,5,10,16,0));
////		dbUtil.updateProgramare(3, dbUtil.readAnimal(2), dbUtil.readPersonalMedical(3), new Date(118,11,25,12,30));
////		dbUtil.deleteProgramare(3);
//		
//		dbUtil.sortareDupaData();
//		
//		
//		
//		
////		afisez programarea cu ordinul = 0 a animalului cu id = 3 
////		System.out.println(dbUtil.readAnimal(3).getProgramares().get(0).getData());
//		
////		dbUtil.printAllAnimalsFromDB();
//		
//		dbUtil.closeEntityManager();
//		
		LongRunningTask longRunningTask = new LongRunningTask();
		longRunningTask.setOnCompleteListener(new OnCompleteListener() {

			@Override
			public void onCompleteListener() {
				System.out.println("Finish!");
			}
		});

		System.out.println("Starting the application.");

		singletone();

//		Socket socket = new Socket("10.146.1.104", 5000);
//		BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//		PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);
//		String echoString;
//		String response;
//		File file = new File(
//				"\"C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\PetClinicTest\\Animals.txt\"");
//		Scanner sc = new Scanner(file);
//		while (sc.hasNextLine()) {
//			System.out.println("Read string to be echo");
//			echoString = sc.nextLine();
//			stringToEcho.println(echoString);
//				response = echoes.readLine();
//				System.out.println(response);
//			}
//
//			ServerSocket serverSocket;
//			try {
//				serverSocket = new ServerSocket(5000);
//				while (true) {
//					new Echoer(serverSocket.accept()).start();
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

			launch(args);
			longRunningTask.run();
		}
	}

	interface OnCompleteListener {
		public void onCompleteListener();
	}

class LongRunningTask implements Runnable{
	private OnCompleteListener onCompleteListener;

	public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(1*1000);
			onCompleteListener.onCompleteListener();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}