package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class SecondController {
	
	@FXML
	private TextField date;
	@FXML
	private TextArea diagnostic;
	@FXML
	private Button ok;
	
	Stage secondStage = new Stage();
	
	public void initialize(URL location, ResourceBundle resources) {
		ok.setOnAction(actionEvent -> secondStage.close());
	}
	
	public void secondStage() {
		BorderPane root2;
		try {
			root2 = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/Scene2.fxml"));
			Scene scene2 = new Scene(root2, 400, 400);
			secondStage.setScene(scene2);
			secondStage.show();
//			Connection conn = null;
//			PreparedStatement ps = null;			
//			try {
//				conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/petshop", "root", "1q2w3e");
//				String data = date.getText();
//				String diag = diagnostic.getText();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
