package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import util.DatabaseUtil;
import model.Animal;
import model.Diagnostic;
import model.Programare;

public class MainController implements Initializable {

	Stage mainStage = new Stage();
	@FXML
	private ListView<String> listView;

	public void populateMainListView() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDBList);
		listView.setItems(animalNamesList);
		listView.refresh();
		
		db.closeEntityManager();
	}

	@FXML
	private TableView<Diagnostic> tabel;
	@FXML
	private TableColumn<Diagnostic, String> dateId = new TableColumn<Diagnostic, String>();
	@FXML
	private TableColumn<Diagnostic, String> diagnosticId = new TableColumn<Diagnostic, String>();
	@FXML
	private ImageView img;
	@FXML
	private TextField name;
	@FXML
	private TextField age;
	@FXML
	private TextField weight;
	@FXML
	private TextField owner;
	@FXML
	private TextField phone;
	@FXML
	private TextField tip;
	@FXML
	private TextField mail;
	@FXML
	private TextField rasa;
	@FXML
	private Text nameText;
	@FXML
	private Text ageText;
	@FXML
	private Text weightText;
	@FXML
	private Text phoneText;
	@FXML
	private Text ownerText;
	@FXML
	private Text mailText;
	@FXML
	private Text breedText;
	@FXML
	private Button cancel;
	@FXML
	private Button done;

	
	@FXML
	public void handleMouseClick(MouseEvent event) {
		dateId.setCellValueFactory(new PropertyValueFactory<Diagnostic, String>("Date"));
		diagnosticId.setCellValueFactory(new PropertyValueFactory<>("Diagnostic"));
		String s = listViewDate.getSelectionModel().getSelectedItem();
		LocalDate date = selectDate.getValue();
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Programare> programari = (List<Programare>) db.progamariList();
		List<Diagnostic> diagnostice = (List<Diagnostic>) db.diagnosticeList();
		
		
		Map<Integer, Programare> programarii = new HashMap<>();
		for (Programare p : programari)
			programarii.put(p.getIdProgramare(), p);
		for(Programare p : programarii.values())
//		for (Programare p : programari)
			if (new SimpleDateFormat("yyyy-MM-dd").format(p.getData()).equals(date.toString()))
				if (p.getData().toString().substring(11, 16).equals(s.substring(0, 5))) {
					ObservableList<Diagnostic> diagnostics = FXCollections.observableArrayList();
					for (Diagnostic d : diagnostice)
						if (p.getAnimal().getIdAnimal() == d.getAnimal().getIdAnimal())
							diagnostics.add(d);
					name.setText(p.getAnimal().getNume());
					age.setText(Integer.toString(p.getAnimal().getVarsta()));
					weight.setText(Double.toString(p.getAnimal().getGreutate()));
					owner.setText(p.getAnimal().getProprietar());
					phone.setText(p.getAnimal().getTelefon());
					mail.setText(p.getAnimal().getMail());
					tip.setText(p.getTip());
					rasa.setText(p.getAnimal().getRasa());
					if (p.getAnimal().getRasa().equals("caine"))
						try {
							img.setImage(new Image(
									new FileInputStream("C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\doggo.jpeg"), 200,
									200, false, false));
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}
					else
						try {
							img.setImage(new Image(
									new FileInputStream("C:\\Users\\Raluca\\Desktop\\Facultate\\MIP\\cat.jpeg"), 200,
									200, false, false));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					name.setVisible(true);
					age.setVisible(true);
					weight.setVisible(true);
					owner.setVisible(true);
					phone.setVisible(true);
					mail.setVisible(true);
					tip.setVisible(true);
					rasa.setVisible(true);
					nameText.setVisible(true);
					ageText.setVisible(true);
					weightText.setVisible(true);
					ownerText.setVisible(true);
					phoneText.setVisible(true);
					mailText.setVisible(true);
					breedText.setVisible(true);
					tabel.setVisible(true);
					dateId.setVisible(true);
					diagnosticId.setVisible(true);
					tabel.setItems(diagnostics);
				}
	}

	@FXML
	private TextField display;
	@FXML
	private DatePicker selectDate;
	@FXML
	private ListView<String> listViewDate;

	@FXML
	private void getDateAction(ActionEvent event) {
		LocalDate date = selectDate.getValue();
		if (date != null) {
			DatabaseUtil db = new DatabaseUtil();
			db.setup();
			db.startTransaction();
			List<Programare> programari = db.progamariList();
			ObservableList<String> dates = FXCollections.observableArrayList();
			for (Programare p : programari)
				if (new SimpleDateFormat("yyyy-MM-dd").format(p.getData()).equals(date.toString()) && p.getCheck() == 0)
					dates.add(p.getData().toString().substring(11, 16) + " " + p.getTip());
//			Collections.sort(dates);
			Collections.sort(dates, (d1,d2) -> d1.compareTo(d2));
			listViewDate.setItems(dates);
			listViewDate.refresh();
			db.closeEntityManager();
		} else {
			display.setText("");
		}

	}

	public ObservableList<String> getAnimalName(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a : animals)
			names.add(a.getNume());
		return names;
	}

	public ObservableList<String> getDiagnotics(List<Diagnostic> diagnostics) {
		ObservableList<String> diagnoticss = FXCollections.observableArrayList();
		for (Diagnostic d : diagnostics)
			diagnoticss.add(d.getDate() + d.getDiagnostic());
		return diagnoticss;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		populateMainListView();
		name.setVisible(false);
		age.setVisible(false);
		weight.setVisible(false);
		owner.setVisible(false);
		phone.setVisible(false);
		mail.setVisible(false);
		tip.setVisible(false);
		rasa.setVisible(false);
		nameText.setVisible(false);
		ageText.setVisible(false);
		weightText.setVisible(false);
		ownerText.setVisible(false);
		phoneText.setVisible(false);
		mailText.setVisible(false);
		breedText.setVisible(false);
		tabel.setVisible(false);
		cancel.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
//				String s=listViewDate.getSelectionModel().getSelectedItem();
//				DatabaseUtil db = new DatabaseUtil();
//				db.setup();
//				db.startTransaction();
//				List<Programare> programari = (List<Programare>) db.progamariList();
//				for (Programare p : programari) 
//					if(p.getData().toString().substring(11, 16).equals(s.substring(0, 5)))
//						//p.setCheck(1);
//				db.updateProgramare(p.getIdProgramare(), p.getAnimal(), p.getPersonalmedical(), p.getData(),1);
//					theQuery("update PetShop.Programare set check = "+1+" where idProgramare = " + p.getIdProgramare());
			}
		});
		done.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
//				String s = listViewDate.getSelectionModel().getSelectedItem();
//				DatabaseUtil db = new DatabaseUtil();
//				db.setup();
//				db.startTransaction();
//				List<Programare> programari = (List<Programare>) db.progamariList();
//				for (Programare p : programari)
//					if (p.getData().toString().substring(11, 16).equals(s.substring(0, 5)))
//						p.setCheck(1); 
//						db.updateProgramare(p.getIdProgramare(), p.getAnimal(), p.getPersonalmedical(), p.getData(),1);
				if(selectDate.getValue() != null)	
				{SecondController sc = new SecondController();
					sc.secondStage();}
				else
					System.exit(0);
			}
		});

	}
	
	public void mainStage() {
		BorderPane root1;
		try {
			root1 = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene1 = new Scene(root1, 850, 620);
			mainStage.setScene(scene1);
			mainStage.show();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void theQuery(String query) {
		Connection conn = null;
		try {
			conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/petshop", "root", "1q2w3e");
			PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
			preparedStmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Query Executed");
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
	}
}
