package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class Login implements Initializable {
	@FXML
	private TextField username;
	@FXML
	private PasswordField pass;
	@FXML
	private Button login;

	public void initialize(URL location, ResourceBundle resources) {
		login.setOnAction(e -> {
			if(username.getText().equals("Raluca") && pass.getText().equals("1q2w3e")) {
				MainController mc = new MainController();
				mc.mainStage();
			} else {
				username.clear();
				username.setText("");
				pass.clear();
				pass.setText("");
			}
		});

	}

}