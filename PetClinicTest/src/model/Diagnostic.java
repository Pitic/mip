package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the diagnostic database table.
 * 
 */
@Entity
@NamedQuery(name="Diagnostic.findAll", query="SELECT d FROM Diagnostic d")
public class Diagnostic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idDiagnostic;

	@Lob
	private String date;

	@Lob
	private String diagnostic;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="fk_id_Animal")
	private Animal animal;

	public Diagnostic() {
	}

	public int getIdDiagnostic() {
		return this.idDiagnostic;
	}

	public void setIdDiagnostic(int idDiagnostic) {
		this.idDiagnostic = idDiagnostic;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

}