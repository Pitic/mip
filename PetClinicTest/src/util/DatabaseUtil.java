package util;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Diagnostic;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetClinicTest");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	public void savePersonalMedical(Personalmedical medic) {
		entityManager.persist(medic);
	}

	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}

	public void setup()
	{
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("PetClinicTest");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

//	incarca obiectele in baza de date	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

//	inchide baza de date	
	public void closeEntityManager() {
		entityManager.close();
	}

	/**
	 * Prints all animals from database
	 */
//	afiseaza toate animalele din baza de date : nume si id	
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class).getResultList();
		for (Animal animal : results) {
			System.out.println("Animal :" + animal.getNume() + " had ID: " + animal.getIdAnimal());
		}
	}

	/**
	 * 
	 * Create an animal.
	 * 
	 * @param name   name for new animal
	 * @param breed     new animal breed
	 * @param weight weight for new animal
	 * @param mail	owner mail
	 * @param dataNasterii    animal birthday
	 * @param proprietar	owner name
	 * @param telefon owner telephone number
	 */
//	creeaza un animal	
	public void createAnimal(String name, String breed, double weight, String mail, Date dataNasterii, String proprietar, String telefon) {
		Animal nou = new Animal();
		startTransaction();
		nou.setRasa(breed);
		nou.setNume(name);
		nou.setGreutate(weight);
		LocalDate birthday = dataNasterii.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		nou.setVarsta(Period.between(birthday, LocalDate.now()).getYears());
		nou.setMail(mail);
		nou.setProprietar(proprietar);
		nou.setTelefon(telefon);
		nou.setDataNasterii(dataNasterii);
		saveAnimal(nou);
		commitTransaction();
	}

//	afiseaza animalul care are id-ul trimis ca parametru	
//	public void readAnimal(int id) { 
//		Animal animal = entityManager.find(Animal.class, id);
//		System.out.println("Animal :" + animal.getNume() + " had ID: " + animal.getIdAnimal());
//	}

	/**
	 * 
	 * Read an animal.
	 * 
	 * @param id id for animal which we want to read
	 */
//	returneaza animalul care are id-ul trimis ca parametru	
	public Animal readAnimal(int id) {
		return entityManager.find(Animal.class, id);
	}

	/**
	 * 
	 * Update an animal.
	 * 
	 * @param id     id for animal which we want to update
	 * @param name   new name
	 * @param breed	 new breed
	 * @param weight new weight
	 * @param mail    new owner mail
	 * @param dataNasterii new birthday
	 * @param proprietar new owner name
	 * @param telefon new owner telephone number
	 */
//	modifica animalul care are id-ul trimis ca parametru cu datele trimise tot ca parametrii	
	public void updateAnimal(int id, String name, String breed, double weight, String mail, Date dataNasterii, String proprietar, String telefon) {
		Animal animal = entityManager.find(Animal.class, id);
		startTransaction();
		animal.setRasa(breed);
		animal.setNume(name);
		animal.setGreutate(weight);
		LocalDate birthday = dataNasterii.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		animal.setVarsta(Period.between(birthday, LocalDate.now()).getYears());
		animal.setProprietar(proprietar);
		animal.setTelefon(telefon);
		animal.setDataNasterii(dataNasterii);
		animal.setMail(mail);
		saveAnimal(animal);
		commitTransaction();
	}

	/**
	 * 
	 * Delete an animal.
	 * 
	 * @param id id for animal which we want to delete
	 */
//	sterge animalul care are id-ul trimis ca parametru	
	public void deleteAnimal(int id) {
		Animal animal = entityManager.find(Animal.class, id);
		startTransaction();
		entityManager.remove(animal);
		commitTransaction();
	}

	/**
	 * 
	 * Create a doctor.
	 * 
	 * @param lastname        lastname for new doctor
	 * @param firstname      firstname for new doctor
	 * @param telephone telephone for new doctor
	 * @param timetable	timetable for new doctor
	 */
//	creeaza un personal medical	
	public void createPersonalMedical(String lastname, String firstname, String telephone, String timetable) {
		Personalmedical doctor = new Personalmedical();
		startTransaction();
		doctor.setPrenume(firstname);
		doctor.setNume(lastname);
		doctor.setTelefon(telephone);
		doctor.setProgram(timetable);
		savePersonalMedical(doctor);
		commitTransaction();
	}

//	afiseaza personalul medical care are id-ul trimis ca parametru	
//	public void readPersonalMedical(int id) {
//		PersonalMedical medic = entityManager.find(PersonalMedical.class, id);
//		System.out.println("Personal medical :" + medic.getNume() + " had ID: " + medic.getIdPersonalMedical());
//	}

	/**
	 * 
	 * Read a doctor.
	 * 
	 * @param id id for doctor which we want to read
	 */
//	returneaza personalul medical care are id-ul trimis ca parametru
	public Personalmedical readPersonalMedical(int id) {
		return entityManager.find(Personalmedical.class, id);
	}

	/**
	 * 
	 * Update a doctor.
	 * 
	 * @param id        id for doctor which we want to update
	 * @param lastname        lastname for doctor which we want to update
	 * @param firstname      firstname for doctor which we want to update
	 * @param telephone telephone for doctor which we want to update
	 * @param timetable	timetable for doctor which we want to update
	 */
//	modifica personalul medical care are id-ul trimis ca parametru cu datele trimise tot ca parametrii
	public void updatePersonalMedical(int id, String lastname, String firstname, String telephone, String timetable) {
		Personalmedical doctor = entityManager.find(Personalmedical.class, id);
		startTransaction();
		doctor.setPrenume(firstname);
		doctor.setNume(lastname);
		doctor.setTelefon(telephone);
		doctor.setProgram(timetable);
		savePersonalMedical(doctor);
		commitTransaction();
	}

	/**
	 * 
	 * Delete a doctor.
	 * 
	 * @param id id for doctor which we want to delete
	 */
//	sterge personalul medical care are id-ul trimis ca parametru
	public void deletePersonalMedical(int id) {
		Personalmedical doctor = entityManager.find(Personalmedical.class, id);
		startTransaction();
		entityManager.remove(doctor);
		commitTransaction();
	}

	/**
	 * 
	 * Create an appointment.
	 * 
	 * @param animal associating the animal for which we want to make an appointment
	 * @param doctor associating the doctor which will work for this appointment
	 * @param date   date for new appointment
	 */
//	creeaza o programare
	public void createProgramare(Animal animal, Personalmedical doctor, Date date) {
		Programare programare = new Programare();
		startTransaction();
		programare.setData(date);
		programare.setPersonalmedical(doctor);
		programare.setAnimal(animal);
		programare.setCheck(0);
		saveProgramare(programare);
		commitTransaction();
	}

	/**
	 * 
	 * Read an appointment.
	 * 
	 * @param id id for appointment which we want to read
	 */
//	returneaza programarea care are id-ul trimis ca parametru
	public Programare readProgramare(int id) {
		return entityManager.find(Programare.class, id);
		
	}


	/**
	 * 
	 * Update an appointment.
	 * 
	 * @param id     id for appointment which we want to update
	 * @param animal associating the new animal for which we want to make an
	 *               appointment
	 * @param doctor associating the new doctor which will work for this appointment
	 * @param date   new date
	 * @param check	went or not to appointment 
	 */
//	modifica programarea care are id-ul trimis ca parametru cu datele trimise tot ca parametrii
	public void updateProgramare(int id, Animal animal, Personalmedical doctor, Date date, int check) {
		Programare programare = entityManager.find(Programare.class, id);
		startTransaction();
		programare.setData(date);
		programare.setPersonalmedical(doctor);
		programare.setAnimal(animal);
		programare.setCheck(check);
		saveProgramare(programare);
		commitTransaction();
	}

	/**
	 * 
	 * Delete an appointment.
	 * 
	 * @param id id for appointment which we want to delete
	 */
//	sterge programarea care are id-ul trimis ca parametru
	public void deleteProgramare(int id) {
		Programare programare = entityManager.find(Programare.class, id);
		startTransaction();
		entityManager.remove(programare);
		commitTransaction();
	}

	/**
	 * 
	 * Sort all appointments by date in ascending order and print them.
	 */
	// sorteaza programarile in ordine crescatoare dupa data si afiseaza numele
	// animalului si al medicului
	public void sortareDupaData() {
		List<Programare> listaProgramari = entityManager.createNativeQuery("SELECT * FROM PetShop.Programare", Programare.class).getResultList();
		for (int i = 0; i < listaProgramari.size() - 1; i++)
			for (int j = i + 1; j < listaProgramari.size(); j++)
				if (listaProgramari.get(i).getData().compareTo(listaProgramari.get(j).getData()) > 0) {
					Programare temp = listaProgramari.get(i);
					listaProgramari.set(i, listaProgramari.get(j));
					listaProgramari.set(j, temp);
				}
		for (int i = 0; i < listaProgramari.size(); i++)
			System.out.println("Animal : " + listaProgramari.get(i).getAnimal().getNume() + "   Medic : "
					+ listaProgramari.get(i).getPersonalmedical().getNume());

	}
	
	public List<Animal> animalList(){
		List<Animal> animalList = entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class).getResultList();
		return animalList;
	}
	
	public List<Personalmedical> personalMedicalList(){
		List<Personalmedical> personalMedicalList = entityManager.createNativeQuery("Select * from PetShop.Personalmedical", Personalmedical.class).getResultList();
		return personalMedicalList;
	}
	
	public List<Programare> progamariList(){
		List<Programare> progamariList = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class).getResultList();
		return progamariList;
	}
	
	public List<Diagnostic> diagnosticeList(){
		List<Diagnostic> diagnosticeList = entityManager.createNativeQuery("Select * from PetShop.Diagnostic", Diagnostic.class).getResultList();
		return diagnosticeList;
	}
	
}
