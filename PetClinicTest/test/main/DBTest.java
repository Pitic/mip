package main;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.Diagnostic;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

class DBTest {

	@Test
	void testEmptyDBAnimal() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>) db.animalList();
		if (animalDBList == null)
			assertFalse(false);
		else
			assertTrue(true);
	}

	@Test
	void testEmptyDBDoctor() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Personalmedical> personalmedicalDBList = (List<Personalmedical>) db.personalMedicalList();
		if (personalmedicalDBList == null)
			assertFalse(false);
		else
			assertTrue(true);
	}

	@Test
	void testEmptyDBProgramare() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Programare> programareDBList = (List<Programare>) db.progamariList();
		if (programareDBList == null)
			assertFalse(false);
		else
			assertTrue(true);
	}

	@Test
	void testEmptyDBDiagnostic() {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		db.startTransaction();
		List<Diagnostic> diagnosticDBList = (List<Diagnostic>) db.diagnosticeList();
		if (diagnosticDBList == null)
			assertFalse(false);
		else
			assertTrue(true);
	}
}
